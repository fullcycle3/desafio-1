FROM golang:1.18 as base

WORKDIR /usr/src/app

COPY . .

RUN go build -o /main main.go


FROM scratch

WORKDIR /usr/src/app

COPY --from=base /main /main

ENTRYPOINT ["/main"]

